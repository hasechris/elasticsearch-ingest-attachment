FROM elasticsearch:7.14.0

RUN /usr/share/elasticsearch/bin/elasticsearch-plugin install --batch ingest-attachment 
#RUN yum-config-manager --add-repo https://download.opensuse.org/repositories/home:/Alexander_Pozdnyakov/CentOS_7/ && \
#    rpm --import https://build.opensuse.org/projects/home:Alexander_Pozdnyakov/public_key && \
#    yum update -y && \
#    yum install -y tesseract-osd tesseract tesseract-langpack-deu tesseract-langpack-eng tesseract-langpack-chi_sim tesseract-langpack-chi_tra tesseract-langpack-dan tesseract-langpack-equ tesseract-langpack-fra tesseract-langpack-ita tesseract-langpack-jpn tesseract-langpack-nld && \
#    yum clean all && \
#    rm -rf /var/cache/yum
